import React, { Component } from 'react'
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'
export class Experiences extends Component {

    state = {
        experiences: []
    }
    handleInputChange = (field, i) => (e) => {
        var experiences = [...this.state.experiences]
        const text = e.target.value
        experiences[i][field] = text
        if (field == 'init' && text.length == 2) experiences[i][field] += '/'
        if (field == 'end' && text.length == 2) experiences[i][field] += '/'
        var infos = { ...this.state.infos }
        if ((field == 'init' && text.length >= 8)) {
            experiences[i][field] = text.substr(0, 7)
        }
        if ((field == 'end' && text.length >= 8)) {
            experiences[i][field] = text.substr(0, 7)
        }
        this.setState({
            experiences
        })
    }
    handleInputFunctionChange = (field, i, j) => (e) => {
        var experiences = [...this.state.experiences]
        const text = e.target.value
        experiences[j][field][i] = text
        this.setState({
            experiences
        })
    }
    addExperience = () => {
        var experiences = [
            ...this.state.experiences
        ]
        experiences.push({
            local: '',
            init: '',
            end: '',
            items: ['']
        })
        this.setState({ experiences })
    }
    addFunction = (i) => {
        var experiences = [
            ...this.state.experiences
        ]
        experiences[i].items.push('')
        this.setState({ experiences })
    }
    removeExperience = (i) => {
        var experiences = [...this.state.experiences]
        experiences.splice(i, 1)
        this.setState({ experiences })

    }
    removeFunction = (i, j) => {
        var experiences = [...this.state.experiences]
        if (experiences[j].items.length > 1) {
            experiences[j].items.splice(i, 1)
            this.setState({ experiences })
        }
    }
    renderFunction = (func, i, j) => {
        return (
            <div className="col-sm-12">
                <div className="col-sm-12 remove">
                    <span>Atuação {i + 1} </span>
                    <i className="fas fa-trash" onClick={() => this.removeFunction(i, j)}></i>
                </div>
                <div className="col-sm-12">
                    <input type="text" className="form-control" placeholder="Atuação" value={this.state.experiences[j].items[i]} onChange={this.handleInputFunctionChange('items', i, j)} />
                </div>
            </div>
        )
    }
    renderExperience = (experience, i) => {
        return (
            <div className="row item-list">
                <div className="col-sm-12 remove">
                    <span>Experiência {i + 1} </span>
                    <i className="fas fa-trash" onClick={() => this.removeExperience(i)}></i>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Local</label>
                    <input type="text" className="form-control" placeholder="Local" value={this.state.experiences[i].local} onChange={this.handleInputChange('local', i)} />
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Cargo</label>
                    <input type="text" className="form-control" placeholder="Cargo" value={this.state.experiences[i].func} onChange={this.handleInputChange('func', i)} />
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Início</label>
                    <input type="text" className="form-control" placeholder="Início" value={this.state.experiences[i].init} onChange={this.handleInputChange('init', i)} />
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Término</label>
                    <input type="text" className="form-control" placeholder="Término" value={this.state.experiences[i].end} onChange={this.handleInputChange('end', i)} />
                </div>
                {experience.items.map((value, index) => this.renderFunction(value, index, i))}
                <div className="add ml-4">
                    <span>Adicionar Atuação</span>
                    <i className="fas fa-plus-circle" onClick={() => this.addFunction(i)}></i>
                </div>
            </div>
        )
    }
    componentDidUpdate() {
        this.props.setExperiences(this.state.experiences)
    }
    render() {
        return (
            <div id="experiences">
                <h4>EXPERIÊNCIAS</h4>
                {this.state.experiences.map(this.renderExperience)}
                <div className="add">
                    <span>Adicionar Experiência</span>
                    <i className="fas fa-plus-circle" onClick={this.addExperience}></i>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setExperiences: (experiences) => dispatch(Creators.setExperiences(experiences))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Experiences)
