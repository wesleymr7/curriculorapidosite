import { takeLatest, all, put } from 'redux-saga/effects'
import { Types, Creators } from '../actionCreators' 
import { getResume } from './resume'

export default function* rootSaga() {
    yield all([
        takeLatest(Types.GET_RESUME_REQUEST, getResume),
    ])
}