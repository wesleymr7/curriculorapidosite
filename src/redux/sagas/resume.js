import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { takeLatest, all, put } from 'redux-saga/effects'
import { Types, Creators } from '../actionCreators' 
import { getResumeByTemplate } from './../../api'

export function* getResume(action) {
    try {
        var data = action
        yield put(Creators.setModal(true))
        const response = yield getResumeByTemplate(data.templateId, data.data)
        yield put(Creators.getResumeSuccess(response.data))
    } catch (e) {
        yield put(Creators.getResumeFailure(e.message))
        console.log(e.message)
    }
}
/*export function* getResume(action) {
    try {
        const login = yield axios.post('http://localhost:8080/customers/login', {
            email: action.email,
            password: action.password
        })
        let token = null
        token = login.data
        localStorage.setItem('token', token)
        const user = jwtDecode(token)
        localStorage.setItem('user', JSON.stringify(user))
        yield put(Creators.signinSuccess(user))
    } catch(e) {
        yield put(Creators.signinFailure('Crendenciais incorretas!'))
    }
}*/