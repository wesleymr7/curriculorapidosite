import { createReducer } from 'reduxsauce'
import { Types } from '../actionCreators'

export const INITIAL_STATE = {
    resume: null,
    loading: false,
    error: null,
    errorMessage: '',
    name: '',
    cep: '',
    street: '',
    number: '',
    district: '',
    state: '',
    city: '',
    phone1: '',
    phone2: '',
    birthday: '',
    civil_state: '',
    email: '',
    nationality: '',
    goal: '',
    schoolings: [],
    experiences: [],
    languages: [],
    courses: [],
    extra_infos: [],
    image: null,
    templateId: null,
    modal: false
}
export const setPersonalData = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        name: action.data.name,
        cep: action.data.cep,
        street: action.data.street,
        number: action.data.number,
        district: action.data.district,
        state: action.data.state,
        city: action.data.city,
        phone1: action.data.phone1,
        phone2: action.data.phone2,
        birthday: action.data.birthday,
        civil_state: action.data.civil_state,
        email: action.data.email,
        nationality: action.data.nationality,
    }
}
export const setTemplateId = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        templateId: action.id
    }
}
export const setSchoolings = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        schoolings: action.schoolings
    }
}
export const setExperiences = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        experiences: action.experiences
    }
}
export const setLanguages = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        languages: action.languages
    }
}
export const setCourses = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        courses: action.courses
    }
}
export const setGoal = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        goal: action.goal
    }
}
export const setImage = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        image: action.image
    }
}
export const setExtraInfos = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        extra_infos: action.extra_infos
    }
}
export const getResumeRequest = ( state =INITIAL_STATE, action) => {
    return {
        ...state,
        loading: true,
        error: false,
        errorMessage: '',
    }
}
export const getResumeSuccess = ( state = INITIAL_STATE, action ) => {
    return {
        ...state,
        resume: action.resume,
        loading: false
    }
}
export const getResumeFailure = ( state = INITIAL_STATE, action ) => {
    return {
        ...state,
        loading: true,
        error: true,
        errorMessage: action.error
    }
}
export const setModal = ( state = INITIAL_STATE, action ) => {
    return {
        ...state,
        modal: action.modal
    }
}

export const HANDLERS = {
    [Types.SET_PERSONAL_DATA]: setPersonalData,
    [Types.SET_SCHOOLINGS]: setSchoolings,
    [Types.SET_EXPERIENCES]: setExperiences,

    [Types.SET_LANGUAGES]: setLanguages,
    [Types.SET_COURSES]: setCourses,
    [Types.SET_EXTRA_INFOS]: setExtraInfos,

    [Types.SET_IMAGE]: setImage,
    [Types.SET_GOAL]: setGoal,

    [Types.GET_RESUME_REQUEST]: getResumeRequest,
    [Types.GET_RESUME_SUCCESS]: getResumeSuccess,
    [Types.GET_RESUME_FAILURE]: getResumeFailure,
    [Types.SET_MODAL]: setModal,
    [Types.SET_TEMPLATE_ID]: setTemplateId

}

export default createReducer(INITIAL_STATE, HANDLERS)