import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import sagas from './sagas'
import reducers from './reducers'

const sagaMiddleware = createSagaMiddleware()
export default createStore(reducers, compose(applyMiddleware(sagaMiddleware),  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()))
//export default createStore(reducers, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(sagas)