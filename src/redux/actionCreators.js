import { createActions } from 'reduxsauce'

export const {
    Types,
    Creators
} = createActions({

    // Actions to customers
    setPersonalData: ['data'],
    setSchoolings: ['schoolings'],
    setExperiences: ['experiences'],

    setLanguages: ['languages'],
    setExtraInfos: ['extra_infos'],
    setCourses: ['courses'],

    setGoal: ['goal'],
    setImage: ['image'],

    // Actions to restaurants admin
    getResumeRequest: ['templateId', 'data'],
    getResumeSuccess: ['resume'],
    getResumeFailure: ['error'],
    setModal: ['modal'],
    setTemplateId: ['id']


})

export default Creators