import React from 'react';
import Swiper from 'react-id-swiper';
// Need to add Pagination, Navigation modules
import { Navigation } from 'swiper/dist/js/swiper.esm'
import { connect } from 'react-redux'
import cx from 'classnames'
import Creators from './redux/actionCreators.js'

class SlideTemplates extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            params: {
                modules: [Navigation],
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },
                renderPrevButton: () => <button className="swiper-button-prev"> <i className="fas fa-angle-left"></i> </button>,
                renderNextButton: () => <button className="swiper-button-next"> <i className="fas fa-angle-right"></i> </button>,
                slidesPerView: 3,
                spaceBetween: 0,
                breakpoints: {
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 0
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 0
                    },
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 0
                    },
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 0
                    }
                }
            },
            templates: [
                {
                    id: 1,
                    img: './template1.png'
                },
                {
                    id: 2,
                    img: './template2.png'
                },
                {
                    id: 3,
                    img: './template3.png'
                },
                {
                    id: 4,
                    img: './template4.png'
                },
                {
                    id: 5,
                    img: './template5.png'
                },
            ],
            selectedTemplate: null
        }
    }
    selectTemplate = (id) => {
        this.setState({ selectedTemplate: id})
        this.props.setTemplateId(id)
    }
    render() {
        return (
            <Swiper {...this.state.params}>
                {
                    this.state.templates.map((t) => {
                        return (
                            <div className="box-template">
                                <img src={t.img} key={t.id} onClick={() => this.selectTemplate(t.id)} className={cx({'img-slide': true, 'template-selected': (this.state.selectedTemplate === t.id)})} />
                                { this.state.selectedTemplate === t.id && <i className={cx('fas fa-check', 'check-template')}></i>}
                            </div>
                        )
                    })
                }
            </Swiper>
        )
    }
}
const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setTemplateId: (id) => dispatch(Creators.setTemplateId(id))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SlideTemplates);