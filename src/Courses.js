import React, { Component } from 'react'
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'

export class Courses extends Component {
    state = {
        courses: [
        ]
    }
    handleInputChange = (field, i) => (e) => {
        var courses = [...this.state.courses]
        courses[i][field] = e.target.value
        this.setState({
            courses
        })
    }
    addCourse = () => {
        var courses = [
            ...this.state.courses
        ]
        courses.push({
            course: '',
            instituition: '',
            year: ''
        })
        courses = courses
        this.setState({ courses })
    }
    removeCourse = (i) => {
        var courses = [...this.state.courses]
        courses.splice(i, 1)
        this.setState({ courses })
    }
    renderCourses = (course, i) => {
        return (
            <div className="row item-list" key={i}>
                <div className="col-sm-12 remove">
                    <span>Curso {i + 1} </span>
                    <i className="fas fa-trash" onClick={() => this.removeCourse(i)}></i>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Curso</label>
                    <input type="text" className="form-control" placeholder="Curso" onChange={this.handleInputChange('course', i)} value={this.state.courses[i].course}/>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Instituição</label>
                    <input type="text" className="form-control" placeholder="Instituição" onChange={this.handleInputChange('instituition', i)} value={this.state.courses[i].instituition}/>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Ano</label>
                    <input type="text" className="form-control" placeholder="Ano" onChange={this.handleInputChange('year', i)} value={this.state.courses[i].year} />
                </div>
            </div>
        )
    }
    componentDidUpdate() {
        this.props.setCourses(this.state.courses)
    }
    render() {
        return (
            <div id="courses">
                <h4>CURSOS</h4>
                {this.state.courses.map(this.renderCourses)}
                <div className="add">
                    <span>Adicionar Curso</span>
                    <i className="fas fa-plus-circle" onClick={this.addCourse}></i>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setCourses: (courses) => dispatch(Creators.setCourses(courses))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Courses)
