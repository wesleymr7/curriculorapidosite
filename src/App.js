import React from 'react';
import SlideTemplates from './SlideTemplates'
import Tips from './Tips'
import PersonalData from './PersonalData'
import Goal from './Goal'
import Schooling from './Schooling';
import Courses from './Courses';
import Languages from './Languages';
import Experiences from './Experiences';
import ProfilePicture from './ProfilePicture';
import ExtraInfos from './ExtraInfos';
import store from './redux'
import { Provider, connect } from 'react-redux'
import Creators from './redux/actionCreators.js'
import PDFView from './PDFView';
class App extends React.Component {

  constructor(props) {
    super(props)
    this.store = store
    this.state = {
      templateId: null
    }
    this.pdfview = React.createRef()
  }
  getResume = () => {
    const state = this.store.getState()
    this.store.dispatch(Creators.getResumeRequest(state.resume.templateId, state.resume))
    this.openModal()
  }
  openModal = () => {
    this.setState({ openModal: true })
  }
  closeModal = () => {
    this.setState({ openModal: false })
  }
  selectTemplate = (id) => {
    this.setState({ templateId: id })
  }
  render() {
    return (
      <Provider store={this.store} >
        <div>
          <div id="header">
            <div className="container">
              <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">
                  <img src="./logo.png" />
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul className="navbar-nav mr-auto">

                  </ul>
                  <a href="#" className="icon-face"><i className="fab fa-facebook-f"></i></a>
                </div>
              </nav>
              <div className="content-header row">
                <div className="col-sm-4">
                  <div className="text-title">
                    <span>Crie seu <strong>currículo</strong> profissional <strong>gratuitamente</strong></span>
                    <div className="button-header-download">
                      <a href="#"><img src="./download_button.png" /></a>
                    </div>
                    <div className="site-call">
                      ou <strong>use nosso site</strong>  <a href="#"><img src="./arrow_button_down.png" /></a>
                    </div>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="app-image"><img src="./app.png" /></div>
                </div>
                <div className="col-sm-0">
                </div>
              </div>
              <div className="bottom-bar-header"><img src="./bottom_bar_header.png" /></div>
            </div>
          </div>
          <div id="call-tips">
            <div className="container">
              <div className="row">
                <div className="col-sm-8 d-flex align-items-center justify-content-between">
                  <h5>Separamos algumas dicas para te ajudar,</h5>
                </div>
                <div className="col-sm-4">
                  <a href="#data" className="btn btn-transparent">Clique aqui e veja mais <img src="./arrow_button_down.png" /></a>
                </div>
              </div>
            </div>
          </div>
          <div id="banner-top">
            <div className="container"></div>
          </div>
          <div id="data" className="container">
            <PersonalData />
            <Goal />
            <Schooling />
            <Courses />
            <Languages />
            <Experiences />
            <ExtraInfos />
            <ProfilePicture />
            <div id="models">
              <h4 style={{ marginBottom: '30px' }}>ESCOLHA UM MODELO PARA SEU CURRÍCULO</h4>
              <div className="row">
                <div className="col-sm-12">
                  <SlideTemplates />
                </div>
                <div className="col-sm-6">
                  <button className="btn btn-transparent btn-block my-resume" onClick={this.getResume}>Gerar meu currículo</button>
                </div>
                <div className="col-sm-6">
                </div>
              </div>
            </div>
            <PDFView />
            <div id="face-comments">
              <div className="row">
                <div className="col-sm-12">
                  <h4><span style={{ color: '#f6431a' }}>Deixe seu Comentário ou Sugestão</span> e Ajude-nos a Melhorar esta Ferramenta </h4>
                </div>
                <div className="col-sm-12"></div>
              </div>
            </div>
            <div id="banner-down">
              <div className="row">
                <div className="col-sm-12">
                </div>
              </div>
            </div>
            <div id="tips">
              <div className="row">
                <div className="col-sm-12">
                  <h4 style={{ marginBottom: '50px' }}>Dicas de Como Criar um<span style={{ color: '#f6431a' }}> Bom Currículo</span></h4>
                </div>
                <div className="col-sm-12">
                  <Tips />
                </div>
              </div>
            </div>
          </div>
          <div id="footer">
            <div className="one">
              <div className="container">
                <div className="row">
                  <div className="col-sm-5 text-right d-flex justify-content-center align-items-center">
                    <span className="text-title">Crie seu <strong>currículo</strong> profissional <strong>gratuitamente</strong></span>
                  </div>
                  <div className="col-sm-2">
                    <img src="./app.png" className="img-fluid app-image" />
                  </div>
                  <div className="col-sm-4 d-flex justify-content-start align-items-center">
                    <a href="#"><img src="./download_button_footer.png" className="img-img-fluid" /></a>
                  </div>
                </div>
              </div>
            </div>
            <div className="two">
              <div className="container">
                <div className="row">
                  <div className="col-sm-12 d-flex justify-content-center align-items-center p-4">
                    <img src="./logo.png" className="img-fluid" />
                  </div>
                </div>
              </div>
            </div>
            <div className="three">
              <div className="container">
                <div className="row d-flex justify-content-between align-items-center">
                  <div>
                    <span>Currículo Rápido - Copyright 2019</span>
                  </div>
                  <div>
                    <a href="#"><i className="fab fa-facebook-f"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;
