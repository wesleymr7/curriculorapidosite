import React, { Component } from 'react';
import PersonalTips from './tips/PersonalTips';
import SchoolingTips from './tips/SchoolingTips';
import CourseTips from './tips/CourseTips';
import LanguageTips from './tips/LanguageTips';
import ExperienceTips from './tips/ExperienceTips';
import ExtraInfoTips from './tips/ExtraInfoTips';

class Tips extends Component {
    state = {}

    render() {
        return (
            <div className="accordion" id="accordionExample">
                <div className="card">
                    <div className="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <span className="title mb-0" >
                           Dados Pessoais e Objetivo
                        </span>
                    </div>

                    <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div className="card-body">
                            <PersonalTips />
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <span className="title mb-0" >
                            Escolaridade
                        </span>
                    </div>
                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div className="card-body">
                            <SchoolingTips />
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <span className="title mb-0" >
                            Cursos
                        </span>
                    </div>
                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div className="card-body">
                            <CourseTips />
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                        <span className="title mb-0" >
                            Idiomas
                        </span>
                    </div>
                    <div id="collapseFour" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div className="card-body">
                            <LanguageTips />
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                        <span className="title mb-0">
                            Experiências Profissionais
                        </span>
                    </div>
                    <div id="collapseFive" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div className="card-body">
                            <ExperienceTips />
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                        <span className="title mb-0" >
                            Informações Adicionais
                        </span>
                    </div>
                    <div id="collapseSix" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div className="card-body">
                            <ExtraInfoTips />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Tips;
