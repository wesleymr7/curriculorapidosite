import React, { Component } from 'react';
import States from './utils/estados.json';
import Cities from './utils/municipios.json';
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'

class PersonalData extends Component {
    state = {
        name: null,
        cep: null,
        street: null,
        number: null,
        district: null,
        state: null,
        city: null,
        phone1: null,
        phone2: null,
        birthday: null,
        civil_state: null,
        email: null,
        nationality: null,
        selectedState: '',
        selectedCity: "Alta Floresta D'Oeste",
        states: [],
        cities: []
    }
    handleInputChange = (field) => (e) => {
        if (field === 'state') {
            const valueState = e.target.value.split('-')
            const cities = Cities.filter(e => e.codigo_uf == valueState[1])
            const value = e.target.value
            this.setState({
                [field]: valueState[0],
                cities,
                selectedState: value,
                selectedCity: cities[0].nome,
                city: cities[0].nome
            })
        } else if (field === 'city') {
            const value = e.target.value
            this.setState({
                [field]: value,
                selectedCity: value
            })
        }
        else {
            this.setState({
                [field]: e.target.value
            })
        }

    }
    initSelects = () => {
        let selectedState = 'RO-11'
        let cities = Cities.filter(c => c.codigo_uf == '11')   
        this.setState({
            states: States,
            cities,
            selectedState,
            selectedCity: cities[0].nome,
            state: selectedState,
            city: cities[0].nome
        })
    }
    componentDidUpdate() {
        this.props.setPersonalData({
            name: this.state.name,
            cep: this.state.cep,
            street: this.state.street,
            number: this.state.number,
            district: this.state.district,
            state: this.state.state,
            city: this.state.city,
            phone1: this.state.phone1,
            phone2: this.state.phone2,
            birthday: this.state.birthday,
            civil_state: this.state.civil_state,
            email: this.state.email,
            nationality: this.state.nationality,
        })
    }
    componentWillMount() {
        this.initSelects()
    }
    render() {
        return (
            <div id="personal-data">
                <h4>DADOS PESSOAIS</h4>
                <form>
                    <div className="row">
                        <div className="col-sm-8">
                            <label for="name">Nome completo</label>
                            <input type="text" className="form-control" placeholder="Nome completo" onChange={this.handleInputChange('name')} value={this.state.name} />
                        </div>
                        <div className="col-sm-4">
                            <label for="nationality">CEP</label>
                            <input type="text" className="form-control" placeholder="CEP" onChange={this.handleInputChange('cep')} value={this.state.cep} />
                        </div>
                        <div className="col-sm-8">
                            <label for="nationality">Rua/Logradouro</label>
                            <input type="text" className="form-control" placeholder="Rua/Logradouro" onChange={this.handleInputChange('street')} value={this.state.street} />
                        </div>
                        <div className="col-sm-4">
                            <label for="nationality">Número</label>
                            <input type="text" className="form-control" placeholder="Número" onChange={this.handleInputChange('number')} value={this.state.number} />
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">Bairro</label>
                            <input type="text" className="form-control" placeholder="Bairro" onChange={this.handleInputChange('district')} value={this.state.district} />
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">Estado</label>
                            <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('state')} value={this.state.selectedState}>
                                {this.state.states.map((state, key) => <option value={state.uf + '-' + state.codigo_uf} key={key}>{state.uf}</option>)}
                            </select>
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">Cidade</label>
                            <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('city')} value={this.state.selectedCity}>
                                {this.state.cities.map((city, key) => <option value={city.nome} key={key}>{city.nome}</option>)}
                            </select>
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">Telefone 1</label>
                            <input type="text" className="form-control" placeholder="Telefone 1" onChange={this.handleInputChange('phone1')} value={this.state.phone1} />
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">Telefone 2</label>
                            <input type="text" className="form-control" placeholder="Telefone 2" onChange={this.handleInputChange('phone')} value={this.state.phone2} />
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">Nascimento</label>
                            <input type="date" className="form-control" placeholder="Nascimento" onChange={this.handleInputChange('birthday')} value={this.state.birthday} />
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">Estado civil</label>
                            <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('civil_state')} value={this.state.civil_state}>
                                <option>Solteiro</option>
                                <option>Casado</option>
                            </select>
                        </div>
                        <div className="col-sm-6">
                            <label for="nationality">E-mail</label>
                            <input type="text" className="form-control" placeholder="E-mail" onChange={this.handleInputChange('email')} value={this.state.email} />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setPersonalData: (data) => dispatch(Creators.setPersonalData(data))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PersonalData);
