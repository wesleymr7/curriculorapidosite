import React, { Component } from 'react'
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'
export class Languages extends Component {
    state = {
        languages:[]
    }
    handleInputChange = (field, i) => (e) => {
        var languages = [...this.state.languages]
        languages[i][field] = e.target.value
        this.setState({
            languages
        })
    }
    addLanguage = () => {
        var languages = [
            ...this.state.languages
        ]
        languages.push({
            language: '',
            read: '',
            write: '',
            speak: ''
        })
        this.setState({ languages })
    }
    removeLanguage = (i) => {
        var languages = [...this.state.languages]
        languages.splice(i, 1)
        this.setState({ languages })
    }
    renderLanguage = (language, i) => {
        return (
            <div className="row item-list" key={i}>
                <div className="col-sm-12 remove">
                    <span>Idioma {i + 1} </span>
                    <i className="fas fa-trash" onClick={() => this.removeLanguage(i)}></i>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Idioma</label>
                    <input type="text" className="form-control" placeholder="Curso" onChange={this.handleInputChange('language', i)} value={this.state.languages[i].language}/>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Escrita</label>
                    <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('write', i)} value={this.state.languages[i].write}>
                        <option>Cursando</option>
                        <option>Completo</option>
                        <option>Interrompido</option>
                    </select>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Leitura</label>
                    <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('read', i)} value={this.state.languages[i].read}>
                        <option>Cursando</option>
                        <option>Completo</option>
                        <option>Interrompido</option>
                    </select>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Fala</label>
                    <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('speak', i)} value={this.state.languages[i].speak}>
                        <option>Cursando</option>
                        <option>Completo</option>
                        <option>Interrompido</option>
                    </select>
                </div>
            </div>
        )
    }
    componentDidUpdate() {
        this.props.setLanguages(this.state.languages)
    }
    render() {
        return (
            <div id="languages">
                <h4>IDIOMAS</h4>
                {this.state.languages.map(this.renderLanguage)}
                <div className="add">
                    <span>Adicionar Idioma</span>
                    <i className="fas fa-plus-circle" onClick={this.addLanguage}></i>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setLanguages: (languages) => dispatch(Creators.setLanguages(languages))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Languages)
