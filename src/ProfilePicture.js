import React, { Component } from 'react'
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'
export class ProfilePicture extends Component {
    constructor(props) {
        super(props)
        this.upload = React.createRef();
        this.state = {
            file: null
        }
    }

    uploadPhoto = (e) => {
       this.setState({
           file: e.target.files[0]
       })
    }
    componentDidUpdate() {
        this.props.setImage(this.state.file)
        console.log(this.state.file)
    }
    render() {
        return (
            <div id="image">
                <div className="row">
                    <div className="col-sm-12">
                        <input type="file" className="form-control" style={{ display: 'none' }} ref={this.upload} onChange={this.uploadPhoto} />
                    </div>
                    <div className="col-sm-6">
                        <button className="btn btn-transparent btn-block" onClick={() => this.upload.current.click()}>UPLOAD DA FOTO</button>
                    </div>
                    <div className="col-sm-6">
                        <label>{this.state.file && this.state.file.name}</label>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setImage: (image) => dispatch(Creators.setImage(image))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfilePicture)
