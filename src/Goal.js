import React, { Component } from 'react';
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'
class Goal extends Component {
    state = {
        goal: ''
    }
    handleInputChange = (field) => (e) => {
        this.setState({
            [field]: e.target.value
        })
    }
    componentDidUpdate() {
        this.props.setGoal(this.state.goal)
    }
    render() {
        return (
            <div id="goal">
                <h4>OBJETIVO</h4>
                <label for="exampleFormControlTextarea1">Descreva seu objetivo e pretensões</label>
                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" onChange={this.handleInputChange('goal')}>{this.state.goal}</textarea>
            </div>
        );
    }
}
const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setGoal: (goal) => dispatch(Creators.setGoal(goal))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Goal)
