//const baseUrl = 'https://curriculorapido.herokuapp.com/api/v1/resume'
//const baseUrl = 'http://localhost:8080/api/v1/resume'
const baseUrl = 'http://192.168.1.2:8080/api/v1/resume'
const ProfileImage = require('./../utils/profileImage.json')

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}
const convertArraysToString = (data) => {
    delete data.loading
    delete data.error
    delete data.errorMessage
    delete data.resume
    delete data.templateId
    delete data.modal
    var courses = {}
    var schoolings = {}
    var experiences = {}
    var extra_infos = {}
    var languages = {}
    courses['courses'] = (data && data.courses) ? data.courses : null
    experiences['experiences'] = (data && data.experiences) ? data.experiences : null
    schoolings['schoolings'] = (data && data.schoolings) ? data.schoolings : null
    extra_infos['extra_infos'] = (data && data.extra_infos) ? data.extra_infos : null
    languages['languages'] = (data && data.languages) ? data.languages : null
    if (data && data.courses) data.courses = JSON.stringify(courses)
    if (data && data.experiences) data.experiences = JSON.stringify(experiences)
    if (data && data.schoolings) data.schoolings = JSON.stringify(schoolings)
    if (data && data.extra_infos) data.extra_infos = JSON.stringify(extra_infos)
    if (data && data.languages) data.languages = JSON.stringify(languages)
    return data
}
export async function getResumeByTemplate(templateId, data) {
    try {
        const newData = convertArraysToString(data)
        var body = new FormData()
        Object.keys(newData).forEach(key => {
            if(key == 'image' && !newData['image']) {
                body.append('image', b64toBlob(ProfileImage.profile, 'image/png'))
            } else {
                body.append(key, newData[key]);
            }
        })
        for (var p of body) {
            console.log(p)
        }
        const response = await fetch(baseUrl + '/' + templateId, {
            method: "POST",
            body
        })
        const responseJson = await response.json()
        return responseJson
    } catch (e) {
        console.log(e.message)
    }
}