import React, { Component } from 'react'
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'

export class ExtraInfos extends Component {
    state = {
        extra_infos: []
    }
    handleInputChange = (field, i) => (e) => {
        var extra_infos = [...this.state.extra_infos]
        extra_infos[i] = e.target.value
        this.setState({
            extra_infos
        })
    }
    addExtraInfo = () => {
        var extra_infos = [
            ...this.state.extra_infos
        ]
        extra_infos.push('')
        this.setState({ extra_infos })
    }
    removeExtraInfo = (i) => {
        var extra_infos = [...this.state.extra_infos]
        extra_infos.splice(i, 1)
        this.setState({ extra_infos })

    }
    renderExtraInfo = (extra, i) => {
        return (
            <div className="row">
                <div className="col-sm-12 remove">
                    <span>Informação {i + 1} </span>
                    <i className="fas fa-trash" onClick={() => this.removeExtraInfo(i)}></i>
                </div>
                <div className="col-sm-12">
                    <input type="text" className="form-control" placeholder="Informação adicional" onChange={this.handleInputChange('extra_infos', i)} value={this.state.extra_infos[i]} />
                </div>
            </div>
        )
    }
    componentDidUpdate() {
        this.props.setExtraInfos(this.state.extra_infos)
    }
    render() {
        return (
            <div id="extra-infos">
                <h4>INFORMAÇÕES ADICIONAIS</h4>
                {this.state.extra_infos.map(this.renderExtraInfo)}
                <div className="add">
                    <span>Adicionar Informação</span>
                    <i className="fas fa-plus-circle" onClick={this.addExtraInfo}></i>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setExtraInfos: (extra_infos) => dispatch(Creators.setExtraInfos(extra_infos))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ExtraInfos)
