import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Creators } from './redux/actionCreators'
import $ from 'jquery'
import { pdfjs } from 'react-pdf';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;




class PDFView extends Component {
    state = {
        numPages: null,
        pageNumber: 1,
        pdf: null
    }

    onDocumentLoadSuccess = ({ numPages }) => {
        this.setState({ numPages })
    }
    componentWillReceiveProps(props) {
        if (props.resume.modal) {
            console.log(true)
            $('#exampleModal').modal('show', {
                keyboard: false,
                backdrop: 'static'
            })
        } else {
            console.log(false)
            $('#exampleModal').modal('hide')
        }

        if (!props.resume.loading) {
            this.setState({
                pdf: this.props.resume.resume
            })
        }
    }
    closeModal = () => {
        this.props.setModal(false)
    }
    componentDidMount() {
        $('#exampleModal').on('hide.bs.modal', (e) => {
            this.closeModal()
        })
    }
    render() {
        const { numPages } = this.state;
        return (
            <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" backdrop="static" keyboard="false">
                <div className="modal-dialog modal-xl" role="document" >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Currículo</h5>
                        </div>
                        <div className="modal-body d-flex justify-content-center align-items-center">
                            {!this.props.resume.loading && this.props.resume.resume &&
                                <object className="pdf" data={`data:application/pdf;base64,${this.props.resume.resume}`} type="application/pdf" >
                                    <p>Seu site não suporta visualização do arquivo </p>
                                    <a className="btn btn-primary btn-block" href={`data:application/pdf;base64,${this.props.resume.resume}`} download>Download</a>
                                </object>
                            }
                            {this.props.resume.loading && !this.props.resume.error && <div className="spinner-border text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>}
                            {this.props.resume.error && <div class="alert alert-danger" role="alert">
                                Ocorreu um erro. Verifique seus dados informados!
                            </div>}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={this.closeModal}>Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        resume: state.resume
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setModal: (modal) => dispatch(Creators.setModal(modal))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PDFView)
