import React, { Component } from 'react'
import { connect } from 'react-redux'
import Creators from './redux/actionCreators.js'
export class Schooling extends Component {
    state = {
        schoolings: [
        ]
    }
    handleInputChange = (field, i) => (e) => {
        var schoolings = [...this.state.schoolings]
        schoolings[i][field] = e.target.value
        this.setState({
            schoolings
        })
    }
    addSchooling = () => {
        var schoolings = [
            ...this.state.schoolings
        ]
        schoolings.push({
            level: 'Ensino Fundamental',
            status: 'Cursando',
            instituition: '',
            course: '',
            duration: '',
            conclusionMonth: '',
            conclusionYear: ''
        })
        this.setState({ schoolings })
    }
    removeSchooling = (i) => {
        var schoolings = [...this.state.schoolings]
        schoolings.splice(i, 1)
        this.setState({ schoolings })
    }
    componentDidUpdate() {
        this.props.setSchoolings(this.state.schoolings)
    }
    renderSchooling = (schooling, i) => {
        return (
            <div className="row item-list" key={i}>
                <div className="col-sm-12 remove">
                    <span>Experiência {i + 1} </span>
                    <i className="fas fa-trash" onClick={() => this.removeSchooling(i)}></i>
                </div>
                <div className="col-sm-6">
                    <label for="nationality"></label>
                    <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('level', i)} value={this.state.schoolings[i].level}>
                        <option>Ensino Fundamental</option>
                        <option>Ensino Médio</option>
                    </select>
                </div>
                <div className="col-sm-6">
                    <label for="nationality"></label>
                    <select className="form-control" id="exampleFormControlSelect1" onChange={this.handleInputChange('status', i)} value={this.state.schoolings[i].status}>
                        <option>Cursando</option>
                        <option>Completo</option>
                        <option>Interrompido</option>
                    </select>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Curso</label>
                    <input type="text" className="form-control" placeholder="Curso" onChange={this.handleInputChange('course', i)} value={this.state.schoolings[i].course} />
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Duração em anos</label>
                    <input type="text" className="form-control" placeholder="Duração em anos" onChange={this.handleInputChange('duration', i)} value={this.state.schoolings[i].duration} />
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Mês de conclusão</label>
                    <input type="text" className="form-control" placeholder="Mês de conclusão" onChange={this.handleInputChange('conclusionMonth', i)} value={this.state.schoolings[i].conclusionMonth} />
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Ano de conclusão</label>
                    <input type="text" className="form-control" placeholder="Ano de conclusão" onChange={this.handleInputChange('conclusionYear', i)} value={this.state.schoolings[i].conclusionYear}/>
                </div>
                <div className="col-sm-6">
                    <label for="nationality">Instituição</label>
                    <input type="text" className="form-control" placeholder="Instituição" onChange={this.handleInputChange('instituition', i)} value={this.state.schoolings[i].instituition} />
                </div>
            </div>
        )
    }
    render() {
        return (
            <div id="schooling">
                <h4>ESCOLARIDADE</h4>
                {this.state.schoolings.map(this.renderSchooling)}
                <div className="add">
                    <span>Adicionar Experiência</span>
                    <i className="fas fa-plus-circle" onClick={this.addSchooling}></i>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {

}
const mapDispatchToProps = (dispatch) => {
    return {
        setSchoolings: (schoolings) => dispatch(Creators.setSchoolings(schoolings))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Schooling)
