import React, { Component } from 'react'

export class SchoolingTips extends Component {
    render() {
        return (
            <div>
                <strong>Escolaridade</strong>
                <p>
                    1 - Descreva todas as graduações que você fez, desde que agreguem para seu objetivo profissional.
        </p>
                <p>Exemplos: </p>
                <p style={{ fontWeight: 'bold', marginTop: 5 }}>- Jovem Aprendiz</p>
                <p style={{ marginTop: 5 }}>Escola Estadual Fictícia da Silva (cursando o 3º ano matutino)</p>
                <p style={{ fontWeight: 'bold', marginTop: 5 }}>- Assistente Administrativo</p>
                <p style={{ marginTop: 5 }}>Jan/2017 a Dez/2018 – Técnico de Administração de Empresas
        ETEC Camargo Aranha, Centro Paula Souza (cursando o 4º semestre noturno)
</p>
                <p style={{ fontWeight: 'bold', marginTop: 5 }}>- Analista Financeiro Pleno</p>
                <p style={{ marginTop: 5 }}>Jul/2017 a Jul/2019 – MBA em Gestão Financeira
Fundação Getulio Vargas, FGV (cursando 3º semestre noturno)</p>
                <p style={{ marginTop: 5 }}>Jan/2014 a Dez/2015 – Tecnólogo em Gestão Financeira
Universidade Anhanguera (concluído)</p>
<a target="_blank" href="https://www.vagas.com.br/profissoes/dicas/formacao-academica-valorize-no-curriculo/" className="btn btn-block" style={{ background: '#c2d21a'}}>MAIS DICAS</a>
            </div>
        )
    }
}

export default SchoolingTips
