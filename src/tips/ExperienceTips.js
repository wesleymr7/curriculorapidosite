import React, { Component } from 'react'

export class ExperienceTips extends Component {
    render() {
        return (
            <div>
                <strong>Experiências</strong>
                <p>
                    1 - Destaque aquelas funções que mais reforçam seu objetivo profissional;
                            </p>
                <p>
                    2 - Cite alguns projetos que você participou e destaque os principais resultados;
                        </p>
                <p>3 - Tenha em mente que o recrutador não trabalha na sua área e não conhece a empresa na qual você trabalhou, por isso é preciso detalhar bem as atividades que você exercia;</p>
                <p>4 - foque nas palavras-chave dos anúncios da sua área;</p>
                <a target="_blank" href="https://www.vagas.com.br/profissoes/dicas/experiencia-profissional-como-escrever-no-curriculo/" className="btn btn-block" style={{ background: '#c2d21a' }}>MAIS DICAS</a>
            </div>
        )
    }
}

export default ExperienceTips
