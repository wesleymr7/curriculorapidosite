import React, { Component } from 'react'

export class CourseTips extends Component {
    render() {
        return (
            <div>
                <strong>Cursos</strong>
                <p>
                    1 - Informe cursos relevantes para seu atual objetivo profissional.
                        </p>
                <p>
                    2 - Evite cursos mais antigos que cinco anos, a não ser que:
                    <p>- a carga horária é realmente relevante;</p>
                    <p>- o curso é um complemento essencial à sua formação;</p>
                    <p>- ele tem total relevância em relação ao seu atual objetivo;</p>
                    <p>- os conhecimentos adquiridos na época continuam relevantes na atualidade;</p>
                    <p>- tais conhecimentos não passaram por muitas atualizações críticas.</p>
                </p>
                <a target="_blank" href="https://blog.curriculum.com.br/manual-da-recolocacao-profissional/quais-cursos-complementares-incluir-no-curriculo-e-como/" className="btn btn-block" style={{ background: '#c2d21a'}}>MAIS DICAS</a>
            </div>
        )
    }
}

export default CourseTips
