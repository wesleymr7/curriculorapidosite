import React, { Component } from 'react'

export class ExtraInfoTips extends Component {
    render() {
        return (
            <div>
                <strong>Informações Adicionais</strong>
                <p>
                    1 - Coloque suas premiações e certificações.
                        </p>
                <p>
                    2 - Informe sua disponibilidade.
                        </p>
                <p>
                    3 - Inclua as palestras que você já participou.
                        </p>
                <p>
                    4 - Coloque seus cursos extracurriculares.
                        </p>
                <p>
                    5 - Confirme se você tem habilitação e carro.
                        </p>
                <a target="_blank" href="https://www.tuacarreira.com/informacoes-adicionais-curriculo/" className="btn btn-block" style={{ background: '#c2d21a' }}>MAIS DICAS</a>
            </div>
        )
    }
}

export default ExtraInfoTips
