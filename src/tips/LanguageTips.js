import React, { Component } from 'react'

export class LanguageTips extends Component {
    render() {
        return (
            <div>
                <strong>Idiomas</strong>
                <p>
                    1 - Seja sincero quanto ao seu nível de conhecimento da língua;
                </p>
                <p>
                    2 - Como autoavaliar a fluência do seu idioma?
                    <p>- BÁSICO: leve compreensão do idioma;</p>
                    <p>- INTERMEDIÁRIO: consegue iniciar uma conversa;</p>
                    <p>- AVANÇADO: fala bem e domina a leitura e a escrita;</p>
                    <p>- FLUENTE: entendimento total da língua;</p>
                </p>
                <a target="_blank" href="https://www.roberthalf.com.br/blog/curriculo/melhor-maneira-de-descrever-os-idiomas-no-curriculo" className="btn btn-block" style={{ background: '#c2d21a' }}>MAIS DICAS</a>
            </div>
        )
    }
}

export default LanguageTips
