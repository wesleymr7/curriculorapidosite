import React, { Component } from 'react';

class PersonalTips extends Component {
    render() {
        return (
            <div>
                <strong>E-mail</strong>
                <p>
                    1 - Cuidado com os nomes que você usa em e-mails. Evite utilizar apelidos ou nomes relacionados a personagens para não
                    passar uma impressão negativa ao avaliador.
                        </p>
                <strong>Objetivo</strong>
                <p>
                    1 - Resuma suas pretensões profissionais de forma simples e direta, especificando a área de atuação e a empresa direcionada.
                        </p>
                <p>
                    2 - Evite fazer afirmações genéricas como "desejo me desenvolver profissionalmente".
                        </p>
                <p>
                    3 - Não utilize frases de efeito.
                        </p>
                <strong>Exemplos: </strong>
                <textarea style={{marginBottom: '5px'}} className="form-control" rows={3}>Tenho a intenção de integrar a equipe de vendas para colaborar na formação de um time coeso, que agregue resultados expressivos e sustentáveis para a empresa.</textarea>
                <textarea style={{marginBottom: '5px'}} className="form-control" rows={3}>Almejo assumir a função de assistente de TI para encontrar soluções para os usuários e enfrentar novos desafios profissionais condizentes com minhas pretensões na carreira.</textarea>
                <textarea style={{marginBottom: '5px'}} className="form-control" rows={3}>Proponho-me a fazer parte da equipe de cozinha para oferecer excelentes experiências gastronômicas aos clientes do restaurante.</textarea>

                <a target="_blank" href="https://www.sbcoaching.com.br/blog/atinja-objetivos/colocar-objetivo-curriculo/" className="btn btn-block" style={{ background: '#c2d21a'}}>MAIS DICAS</a>
            </div>
        );
    }
}

export default PersonalTips;
